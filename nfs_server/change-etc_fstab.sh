#!/bin/bash

# source helper functions
. ../tools/helperfunctions.sh

# source configuration
. ../SAMBA4.conf

# manipulated file
FILE=/etc/fstab
printAndLogMessage "Manipulated file: " ${FILE}

printAndLogMessage "Save original file: " ${FILE}
saveOriginal ${FILE}
logFile ${FILE}

printAndLogMessage "Write to file: " ${FILE}

for CONTAINER in ${OU_USER_CONTAINER_LIST}; do
  echo "# adding exports for ${CONTAINER}" >>${FILE}
  # we drop OU= from container: OU=701036 -> 701036
  SCHOOL_IDENTIFIER=${CONTAINER#OU=}
  for GROUP_IDENTIFIER in ${GROUP_IDENTIFIER_LIST}; do
    echo "#"
    echo "${SAMBA4_HOMES_BASE_DIR}/${SCHOOL_IDENTIFIER}/${GROUP_IDENTIFIER} \
                       ${NFS_EXPORT_DIR}/${SCHOOL_IDENTIFIER}/${GROUP_IDENTIFIER}     none    bind  0  0" >>${FILE}
  done
done

echo "#
" >>${FILE}

logFile ${FILE}
