#!/bin/bash

# source helper functions
. ../tools/helperfunctions.sh

# source configuration
. ../SAMBA4.conf

# manipulated file
FILE=/etc/gssproxy/gssproxy.conf
printAndLogMessage "Manipulated file: " ${FILE}

printAndLogMessage "Save original file: " ${FILE}
saveOriginal ${FILE}
logFile ${FILE}

printAndLogMessage "Write to file: " ${FILE}

echo "
[gssproxy]

[service/nfs-server]
  mechs = krb5
  socket = /run/gssproxy.sock
  cred_store = keytab:/etc/krb5.keytab
  trusted = yes
  kernel_nfsd = yes
  euid = 0

" >${FILE}

logFile ${FILE}
