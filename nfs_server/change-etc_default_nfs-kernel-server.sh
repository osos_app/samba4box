#!/bin/bash

# source helper functions
. ../tools/helperfunctions.sh

# source configuration
. ../SAMBA4.conf

# manipulated file
FILE="/etc/default/nfs-kernel-server"
printAndLogMessage "Manipulated file: "  ${FILE}

printAndLogMessage "Save original file: " ${FILE}
saveOriginal ${FILE}
logFile ${FILE}

printAndLogMessage "Write to file: " ${FILE}

# single quotes '{ here, because double quotes inside !
sed -e '{
	/NEED_SVCGSSD=""/ s/NEED_SVCGSSD=""/NEED_SVCGSSD="yes"/
}' -i ${FILE}

logFile ${FILE}


