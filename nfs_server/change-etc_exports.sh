#!/bin/bash

# source helper functions
. ../tools/helperfunctions.sh

# source configuration
. ../SAMBA4.conf

# manipulated file
FILE=/etc/exports
printAndLogMessage "Manipulated file: " ${FILE}

printAndLogMessage "Save original file: " ${FILE}
saveOriginal ${FILE}
logFile ${FILE}

printAndLogMessage "Write to file: " ${FILE}

if [[ ! "${KERBEROS_SECURITY}" == "" ]]; then
  ## we add semicolon "," to krb5 opttion if not empty to add to export options
  KRB5="${KERBEROS_SECURITY},"
fi

echo "
# export ${NFS_EXPORT_DIR}
${NFS_EXPORT_DIR} ${NFS_ALLOWED_IP_RANGE}(${KRB5}fsid=0,rw,insecure,no_subtree_check,async)
#
#" >>${FILE}

for CONTAINER in ${OU_USER_CONTAINER_LIST}; do
  echo "# adding exports for ${CONTAINER}" >>${FILE}
  # we drop OU= from container: OU=701036 -> 701036
  SCHOOL_IDENTIFIER=${CONTAINER#OU=}
  for GROUP_IDENTIFIER in ${GROUP_IDENTIFIER_LIST}; do
    echo "#"
    echo "${NFS_EXPORT_DIR}/${SCHOOL_IDENTIFIER}/${GROUP_IDENTIFIER} \
                      ${NFS_ALLOWED_IP_RANGE}(${KRB5}rw,nohide,insecure,no_subtree_check,async)" >>${FILE}
  done
done

echo "#
" >>${FILE}

logFile ${FILE}
