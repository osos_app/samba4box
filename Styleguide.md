# Styleguide

The scripts have been written without a styleguide first.

We try to come near the Google styleguide:

https://google.github.io/styleguide/shellguide.html

* [[..]] always
* "${VARIABLE}" nearly always
* intention with 2 spaces
* if [[...]; then in one line
* for ... ; do in one line
* comment for functions