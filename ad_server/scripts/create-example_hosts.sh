#!/bin/bash

## ATTENTION:
## in add-XXXX_from_csv_file.sh
## CREATOR="Administrator"
## PASSWORD="Passw0rd"
## have be be set. If PASSWORD="" it will be asked

## toplevel ou, where all the stuff lives
OU_TOPLEVEL="OU=school,DC=osos,DC=at"

NET_DIRECTORY="net_10"
#NET_DIRECTORY="net_192"

################ SERVER infrastructure ##################

## add hosts from csv file
/bin/bash add-hosts_from_csv_file.sh csv/${NET_DIRECTORY}/school_servers.csv

################ HOSTS infrastructure ##################

## create ou for Floor0 in Container Computers in OU_TOPLEVEL
samba-tool ou create OU=Floor0,OU=Computers,${OU_TOPLEVEL}

## create ou for Room001 in Floor0 in Container Computers in OU_TOPLEVEL
samba-tool ou create OU=R001,OU=Floor0,OU=Computers,${OU_TOPLEVEL}

## create ou for Room002 in Floor0 in Container Computers in OU_TOPLEVEL
samba-tool ou create OU=R002,OU=Floor0,OU=Computers,${OU_TOPLEVEL}

## create ou for Room0802 in Floor0 in Container Computers in OU_TOPLEVEL
samba-tool ou create OU=R080,OU=Floor0,OU=Computers,${OU_TOPLEVEL}

## create ou for Floor1 in Container Computers in OU_TOPLEVEL
samba-tool ou create OU=Floor1,OU=Computers,${OU_TOPLEVEL}

## create ou for Room101 in Floor1 in Container Computers in OU_TOPLEVEL
samba-tool ou create OU=R101,OU=Floor1,OU=Computers,${OU_TOPLEVEL}

## create ou for Room102 in Floor1 in Container Computers in OU_TOPLEVEL
samba-tool ou create OU=R102,OU=Floor1,OU=Computers,${OU_TOPLEVEL}

## add hosts from csv file
/bin/bash add-hosts_from_csv_file.sh csv/${NET_DIRECTORY}/school_hosts.csv
