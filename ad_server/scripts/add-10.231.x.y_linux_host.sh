#!/bin/bash

if [[ "$1" == "" ]]; then
  echo " 
## USAGE:
## add-linux_host.sh HOSTNAME COMPUTEROU IP CREATOR PASSWORD
##
## Examples:
## add server:
## add-linux_host.sh  HOSTNAME  COMPUTEROU                                      IP          CREATOR        PASSWORD
## add-linux_host.sh  fs02      OU=Servers,OU=MANGED_CONTAINER,DC=APP,DC=TSN    10.0.0.17   Administrator  Passw0rd
##
## add normal host:
## add-linux_host.sh  HOSTNAME  COMPUTEROU                                      IP          CREATOR        PASSWORD
## add-linux_host.sh  r213pc50  OU=Computers,OU=MANGED_CONTAINER,DC=APP,DC=TSN  10.2.13.50  Administrator  Passw0rd
##
## ATTENTION:
## 1. has to run on the active directory domain controller, because we use ldbmodify to modify directory
##
## 2. CREATOR has to have rights on the DNS - Server
##    Machines can be joined LATER by an user, which has ONLY rights on COMPUTEROU Container
##
## 3. COMPUTEROU must already exist
##
## 4. REVERSE_IP calculation is just done for network 10.0.0.0/8
"
  exit
fi

AD_HOSTNAME=$(hostname)

HOSTNAME=$1
COMPUTEROU=$2
IP=$3
CREATOR=$4
PASSWORD=$5
if [[ "${PASSWORD}" == "" ]]; then
  echo "Please enter password for user ${CREATOR}"
  read -r PASSWORD
fi

CHECK=$(echo "$IP" | awk 'BEGIN { FS = "." } { print $2 }')
if (("${CHECK}" > 147)); then
  echo "IP is 10.231.xxx.xxx so we reduce it to 10.31.xxx.xxx, WHEN calculate UID-number"
fi

echo "create host ${HOSTNAME} with IP: ${IP} in ${COMPUTEROU}"
samba-tool computer create "${HOSTNAME}" --computerou="${COMPUTEROU}" --description="${HOSTNAME}" --ip-address="${IP}" --username="${CREATOR}" --password="${PASSWORD}"

echo "nfs service principal added"
DOMAIN_SUFFIX=$(samba-tool domain info "${AD_HOSTNAME}" | grep Domain | awk '{print $NF}')
samba-tool spn add nfs/"${HOSTNAME}.${DOMAIN_SUFFIX}" "${HOSTNAME}$" --username="${CREATOR}" --password="${PASSWORD}"
samba-tool spn add nfs/"${HOSTNAME^^}" "${HOSTNAME}$" --username="${CREATOR}" --password="${PASSWORD}"

echo "add host ${HOSTNAME} to DNS reverse zone"
REVERSE_NETWORK=$(echo "$IP" | awk 'BEGIN { FS = "." } { print $1 }')
REVERSE_IP=$(echo "$IP" | awk 'BEGIN { FS = "." } { print $4"."$3"."$2 }')
samba-tool dns add "${AD_HOSTNAME}" "${REVERSE_NETWORK}".in-addr.arpa "${REVERSE_IP}" PTR "${HOSTNAME}.${DOMAIN_SUFFIX}" --username="${CREATOR}" --password="${PASSWORD}"

echo "add uidNumber to machine account"
## we need 10 digits
## we replace the first IP triplet with 2
## and check the second to stay beyond 147 because 2^31-1 = 2 147 483 647 :-(
## 10.3.45.231 -> 2 003 045 231 -> 2003045231

## here we reduce IP to UID $2 -> $2-200
UID_NUMBER=$(echo "$IP" | awk 'BEGIN { FS = "." } { printf "2%.3d%.3d%.3d", $2-200, $3, $4 }')
echo "
dn: CN=${HOSTNAME},${COMPUTEROU}
changetype: modify
add: uidNumber
uidNumber: ${UID_NUMBER}
" >/tmp/"${HOSTNAME}.ldif"

ldbmodify -H /var/lib/samba/private/sam.ldb /tmp/"${HOSTNAME}.ldif" --user="${CREATOR}" --password="${PASSWORD}"
