#!/bin/bash

## ATTENTION:
## in add-XXXX_from_csv_file.sh
## CREATOR="Administrator"
## PASSWORD="Passw0rd"
## have be be set. If PASSWORD="" it will be asked

SCHOOLNUMBER=$1

## for differnet schools we need different UIDs
## UIDs have to bee gigger than 5 000 000
## and start number is          ^
##                                nnn nnn
## will be filled up
## ATTENTION:
## UIDs here AND in
START_NUMBER_UID=$2

CSV_FILE=$3

if [[ "${SCHOOLNUMBER}" == "" ]]; then
  echo "Please call script $0 with:"
  echo "* your school-number"
  echo "* your startnumber for UIDs, which must fit to entries in "
  echo "* your csv - file"
  echo "Example: $0 701036 5 csv/school_701036_users.csv"
  exit
fi

## toplevel ou, where all the stuff lives
OU_TOPLEVEL="OU=SCHOOL"

############### users #####################

## second level OU number 1
SCHOOL_NUMBER_OU="OU=${SCHOOLNUMBER}"
## primary linux group, where all users from SCHOOL_NUMBER_OU belong too
PRIMARY_LINUX_GROUP_USERS="School_UsersSNR${SCHOOLNUMBER}"
PRIMARY_LINUX_GROUP_USERS_UID="${START_NUMBER_UID}000001"

## primary linux group, where all examinees from SCHOOL_NUMBER_OU belong too
PRIMARY_LINUX_GROUP_EXAMINEES="School-ExamineeSNR${SCHOOLNUMBER}"
PRIMARY_LINUX_GROUP_EXAMINEE_UID="${START_NUMBER_UID}000002"

## secondary linux group, where all teachers from SCHOOL_NUMBER_OU belong too
SECONDARY_LINUX_GROUP_TEACHERS="School-LehrerSNR${SCHOOLNUMBER}"
SECONDARY_LINUX_GROUP_TEACHERS_UID="${START_NUMBER_UID}000003"

## secondary linux group, where all teachers & examinees from SCHOOL_NUMBER_OU belong too
SECONDARY_LINUX_GROUP_TEACHERS_EXAMINEES="School-LehrerExamineeSNR${SCHOOLNUMBER}"
SECONDARY_LINUX_GROUP_TEACHERS_EXAMINEES_UID="${START_NUMBER_UID}000004"

## create ou for first Container in OU_TOPLEVEL
samba-tool ou create "${SCHOOL_NUMBER_OU},${OU_TOPLEVEL}"

## create ou for all users
samba-tool ou create OU=Users,"${SCHOOL_NUMBER_OU},${OU_TOPLEVEL}"

## create ou for administration = Verwaltung
samba-tool ou create OU=v,OU=Users,"${SCHOOL_NUMBER_OU},${OU_TOPLEVEL}"

## create ou for teachers = Lehrer
samba-tool ou create OU=l,OU=Users,"${SCHOOL_NUMBER_OU},${OU_TOPLEVEL}"

## create ou for pupils = Schüler
samba-tool ou create OU=s,OU=Users,"${SCHOOL_NUMBER_OU},${OU_TOPLEVEL}"
samba-tool ou create OU=1a,OU=s,OU=Users,"${SCHOOL_NUMBER_OU},${OU_TOPLEVEL}"
samba-tool ou create OU=1b,OU=s,OU=Users,"${SCHOOL_NUMBER_OU},${OU_TOPLEVEL}"

## create ou for Examinees
samba-tool ou create OU=e,OU=Users,"${SCHOOL_NUMBER_OU},${OU_TOPLEVEL}"

## create ou for groups
samba-tool ou create OU=Groups,"${SCHOOL_NUMBER_OU},${OU_TOPLEVEL}"

## create primary linux group for users for school01 with unused nis-domain
samba-tool group add "${PRIMARY_LINUX_GROUP_USERS}" --groupou=OU=Groups,"${SCHOOL_NUMBER_OU},${OU_TOPLEVEL}" --gid-number="${PRIMARY_LINUX_GROUP_USERS_UID}" --nis-domain="NISDOMAIN"

## create primary linux group for examinees for school01 with unused nis-domain
samba-tool group add "${PRIMARY_LINUX_GROUP_EXAMINEES}" --groupou=OU=Groups,"${SCHOOL_NUMBER_OU},${OU_TOPLEVEL}" --gid-number="${PRIMARY_LINUX_GROUP_EXAMINEE_UID}" --nis-domain="NISDOMAIN"

## add users from csv file
/bin/bash add-users_from_csv_file.sh "${CSV_FILE}"

## create secondary linux group for teachers for school with unused nis-domain
samba-tool group add "${SECONDARY_LINUX_GROUP_TEACHERS}" --groupou=OU=Groups,"${SCHOOL_NUMBER_OU},${OU_TOPLEVEL}" --gid-number="${SECONDARY_LINUX_GROUP_TEACHERS_UID}" --nis-domain="NISDOMAIN"

## add teachers from file csv -file to group SECONDARY_LINUX_GROUP_TEACHERS
samba-tool group addmembers "${SECONDARY_LINUX_GROUP_TEACHERS}" "teacher_${SCHOOLNUMBER}_01"
samba-tool group addmembers "${SECONDARY_LINUX_GROUP_TEACHERS}" "teacher_${SCHOOLNUMBER}_02"
samba-tool group addmembers "${SECONDARY_LINUX_GROUP_TEACHERS}" "teacher_${SCHOOLNUMBER}_03"
samba-tool group addmembers "${SECONDARY_LINUX_GROUP_TEACHERS}" "teacher_${SCHOOLNUMBER}_04"

## create secondary linux group for teachers & examinees for school with unused nis-domain
samba-tool group add "${SECONDARY_LINUX_GROUP_TEACHERS_EXAMINEES}" --groupou=OU=Groups,"${SCHOOL_NUMBER_OU},${OU_TOPLEVEL}" --gid-number="${SECONDARY_LINUX_GROUP_TEACHERS_EXAMINEES_UID}" --nis-domain="NISDOMAIN"

## add teachers from file csv -file to group SECONDARY_LINUX_GROUP_TEACHERS_EXAMINEES
samba-tool group addmembers "${SECONDARY_LINUX_GROUP_TEACHERS_EXAMINEES}" "teacher_${SCHOOLNUMBER}_01"
samba-tool group addmembers "${SECONDARY_LINUX_GROUP_TEACHERS_EXAMINEES}" "teacher_${SCHOOLNUMBER}_02"
samba-tool group addmembers "${SECONDARY_LINUX_GROUP_TEACHERS_EXAMINEES}" "teacher_${SCHOOLNUMBER}_03"
samba-tool group addmembers "${SECONDARY_LINUX_GROUP_TEACHERS_EXAMINEES}" "teacher_${SCHOOLNUMBER}_04"

## add examinees from file csv -file to group SECONDARY_LINUX_GROUP_TEACHERS_EXAMINEES
samba-tool group addmembers "${SECONDARY_LINUX_GROUP_TEACHERS_EXAMINEES}" "examinee_${SCHOOLNUMBER}_01"
samba-tool group addmembers "${SECONDARY_LINUX_GROUP_TEACHERS_EXAMINEES}" "examinee_${SCHOOLNUMBER}_02"
samba-tool group addmembers "${SECONDARY_LINUX_GROUP_TEACHERS_EXAMINEES}" "examinee_${SCHOOLNUMBER}_03"
samba-tool group addmembers "${SECONDARY_LINUX_GROUP_TEACHERS_EXAMINEES}" "examinee_${SCHOOLNUMBER}_04"
