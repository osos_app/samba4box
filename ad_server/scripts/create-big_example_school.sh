#!/bin/bash

### SERVER infrastructure AND HOSTS infrastructure
### for all schools
/bin/bash create-example_hosts.sh

### school with school number 301056
/bin/bash create-example_users_in_nnnnnn.sh 301056 8 csv/school_301056_users.csv

### school with school number 405016
/bin/bash create-example_users_in_nnnnnn.sh 405016 7 csv/school_405016_users.csv

### school with school number 701076
/bin/bash create-example_users_in_nnnnnn.sh 701076 6 csv/school_701076_users.csv

### school with school number 701036
/bin/bash create-example_users_in_nnnnnn.sh 701036 5 csv/school_701036_users.csv
