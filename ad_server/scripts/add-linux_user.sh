#!/bin/bash

if [[ "$1" == "" ]]; then
  echo " 
	## USAGE:
	## add-linux_host.sh USERNAME PASSWORD USER_OU WINDOWS_HOME_DIR UID_NUMBER LINUX_HOME_DIR PRIMARY_GROUP GID_NUMBER CREATOR PASSWORD
	##
	## Example:
	## add-linux_host.sh   USERNAME   PASSWORD   USER_OU    WINDOWS_HOME_DIR                     UID_NUMBER   LINUX_HOME_DIR                PRIMARY_GROUP   GID_NUMBER   CREATOR         PASSWORD
	## add-linux_host.sh   user01     Passw0rd   OU=Users   \\\\fs01\\users\\701036\\v\\user01   4000001      /home/users/701036/v/user01   Office          4000001      Administrator   Passw0rd
	"
  exit
fi

USERNAME=$1
USER_PASSWORD=$2
USER_OU=$3
WINDOWS_HOME_DIR=$4
UID_NUMBER=$5
LINUX_HOME_DIR=$6
PRIMARY_GROUP=$7
GID_NUMBER=$8
CREATOR=$9
PASSWORD=${10}

if [[ "${PASSWORD}" == "" ]]; then
  echo "Please enter password for administrator ${CREATOR}"
  read -r PASSWORD
fi

echo "add user ${USERNAME} to OU: ${USER_OU}"
samba-tool user create "${USERNAME}" "${USER_PASSWORD}" \
  --given-name "${USERNAME}" \
  --userou "${USER_OU}" \
  --home-drive "H:" \
  --home-directory "${WINDOWS_HOME_DIR}" \
  --uid "${USERNAME}" \
  --nis-domain "NIS_DOMAIN" \
  --uid-number "${UID_NUMBER}" \
  --login-shell "/bin/bash" \
  --unix-home "${LINUX_HOME_DIR}" \
  --gid-number "${GID_NUMBER}" \
  --gecos "${USERNAME}" \
  --username="${CREATOR}" \
  --password="${PASSWORD}"

echo "add user ${USERNAME} to GROUP: ${PRIMARY_GROUP}"
samba-tool group addmembers "${PRIMARY_GROUP}" "${USERNAME}"
