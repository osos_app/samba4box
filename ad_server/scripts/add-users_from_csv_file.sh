#!/bin/bash

##
CREATOR="Administrator"
## if PASSWORD is empty, add-linux_host.sh will ask for a password
PASSWORD="Passw0rd"

CSV_INPUT_FILE=${1}

## CSV -file format
# USERNAME; USER_PASSWORD; USER_OU; WINDOWS_HOME_DIR; UID_NUMBER=; LINUX_HOME_DIR; PRIMARY_GROUP; GID_NUMBER
# for example, have a look at add-linux_host.sh script

while read -r LINE; do
  ## check if LINE starts with # for comment
  if [[ ! "${LINE:0:1}" == "#" && "${LINE}" != "" ]]; then
    echo "##  processing CSV line ${LINE}"
    ## get info from csv file
    USERNAME=$(echo "${LINE}" | awk ' BEGIN { FS = ";" } { print $1 }')
    USER_PASSWORD=$(echo "${LINE}" | awk ' BEGIN { FS = ";" } { print $2 }')
    USER_OU=$(echo "$LINE" | awk ' BEGIN { FS = ";" } { print $3 }')
    WINDOWS_HOME_DIR=$(echo "${LINE}" | awk ' BEGIN { FS = ";" } { print $4 }')
    UID_NUMBER=$(echo "${LINE}" | awk ' BEGIN { FS = ";" } { print $5 }')
    LINUX_HOME_DIR=$(echo "${LINE}" | awk ' BEGIN { FS = ";" } { print $6 }')
    PRIMARY_GROUP=$(echo "${LINE}" | awk ' BEGIN { FS = ";" } { print $7 }')
    GID_NUMBER=$(echo "${LINE}" | awk ' BEGIN { FS = ";" } { print $8 }')

    echo "add user ${USERNAME} from csv"
    ./add-linux_user.sh "${USERNAME}" \
      "${USER_PASSWORD}" \
      "${USER_OU}" \
      "${WINDOWS_HOME_DIR}" \
      "${UID_NUMBER}" \
      "${LINUX_HOME_DIR}" \
      "${PRIMARY_GROUP}" \
      "${GID_NUMBER}" \
      "${CREATOR}" \
      "${PASSWORD}"
  fi
done < "${CSV_INPUT_FILE}"
