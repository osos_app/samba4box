#!/bin/bash

### SERVER infrastructure AND HOSTS infrastructure

## add servers from csv file
/bin/bash add-10.231.x.y_hosts_from_csv_file.sh csv/Verwaltung_7010nn/v_701036_servers.csv

## add hosts from csv file
/bin/bash add-10.231.x.y_hosts_from_csv_file.sh csv/Verwaltung_7010nn/v_701036_hosts.csv

### administration with school number 701036

## create ou for all users
samba-tool ou create OU=Users,OU=VERWALTUNG

## create ou for groups
samba-tool ou create OU=Groups,OU=VERWALTUNG

## create primary linux group for users for school01 with unused nis-domain
samba-tool group add Verwaltung --groupou=OU=Groups,OU=VERWALTUNG --gid-number=6000001 --nis-domain="NISDOMAIN"

## add users from csv file
/bin/bash add-users_from_csv_file.sh csv/Verwaltung_7010nn/v_701036_users.csv
