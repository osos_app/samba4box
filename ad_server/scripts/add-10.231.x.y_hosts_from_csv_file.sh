#!/bin/bash

##
CREATOR="Administrator"
## if PASSWORD is empty, add-linux_host.sh will ask for a password
PASSWORD="Passw0rd"

CSV_INPUT_FILE=${1}

## CSV -file format
# HOSTNAME; COMPUTEROU; IP
# for example, have a look at add-linux_host.sh script

while read LINE; do
  ## check if LINE starts with # for comment
  if [[ ! "${LINE:0:1}" == "#" && ${LINE} != "" ]]; then
    echo "##  processing CSV line ${LINE}"
    ## get info from csv file
    HOSTNAME=$(echo "${LINE}" | awk ' BEGIN { FS = ";" } { print $1 }')
    COMPUTER_OU=$(echo "${LINE}" | awk ' BEGIN { FS = ";" } { print $2 }')
    IP=$(echo "${LINE}" | awk ' BEGIN { FS = ";" } { print $3 }')

    echo "add host ${HOSTNAME} from csv"
    ./add-10.231.x.y_linux_host.sh "${HOSTNAME}" \
      "${COMPUTER_OU}" \
      "${IP}" \
      "${CREATOR}" \
      "${PASSWORD}"

  fi
done < "${CSV_INPUT_FILE}"
