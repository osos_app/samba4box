#!/bin/bash

SAMBA4_DNS_DOMAIN_NAME="app.tsn"
ADMINISTRATOR_PASSWD=""

if [[ "${ADMINISTRATOR_PASSWD}" == "" ]]; then
  echo "Please set Administrator-password in script!"
  echo "and REMOVE after usage!!"
fi

SERVER_CONTAINER="OU=Servers,OU=APP,DC=APP,DC=TSN"

function add-host_to_servers() {
  samba-tool computer show "${1}"
  if samba-tool computer show "${1}"; then
    ./add-linux_host.sh "${1}" "${SERVER_CONTAINER}" "${2}" "Administrator" "${ADMINISTRATOR_PASSWD}"
  else
    echo "${1} already exists, nothing to do :-)"
  fi
}

# START YOUR LIST HERE

## bind9 nameserver
add-host_to_servers name01 10.0.0.1
add-host_to_servers name02 10.0.0.2

## isc-dhcp-servers
add-host_to_servers dhcp01 10.0.0.3
add-host_to_servers dhcp02 10.0.0.4

## tftp boot servers
add-host_to_servers tftp01 10.0.0.5
add-host_to_servers tftp02 10.0.0.6

## laus client install servers
add-host_to_servers laus01 10.0.0.7
add-host_to_servers laus02 10.0.0.8

## openldap servers
add-host_to_servers ldap01 10.0.0.9
add-host_to_servers ldap02 10.0.0.10

## ad - directories,
## will added to directory by install
##
##                  ad01 10.0.0.11
##                  ad02 10.0.0.12

## old nfs/smb - servers

## new nfs/smb - servers
## home dirs teachers
add-host_to_servers hole01 10.0.0.21
add-host_to_servers hole02 10.0.0.22
## homedirs pupils
add-host_to_servers hosu01 10.0.0.23
add-host_to_servers hosu02 10.0.0.24
## xchange for standard user
add-host_to_servers xchg01 10.0.0.25
add-host_to_servers xchg02 10.0.0.26
samba-tool dns add ad01 "${SAMBA4_DNS_DOMAIN_NAME} nfs03 CNAME xchg02.${SAMBA4_DNS_DOMAIN_NAME}" --username=Administrator --password="${ADMINISTRATOR_PASSWD}"
## lehrmaterial
add-host_to_servers lmat01 10.0.0.27
samba-tool dns add ad01 "${SAMBA4_DNS_DOMAIN_NAME} nfs02 CNAME lmat01.${SAMBA4_DNS_DOMAIN_NAME}" --username=Administrator --password="${ADMINISTRATOR_PASSWD}"
samba-tool dns add ad01 "${SAMBA4_DNS_DOMAIN_NAME} nfs04 CNAME lmat01.${SAMBA4_DNS_DOMAIN_NAME}" --username=Administrator --password="${ADMINISTRATOR_PASSWD}"
add-host_to_servers lmat02 10.0.0.28

## apt cacher for dpkg packages
add-host_to_servers apca01 10.0.0.41

## old smb01 server
add-host_to_servers smb01 10.0.0.52

## IMM management interfaces for PVE
add-host_to_servers imm-pfsense 10.0.0.60
add-host_to_servers imm-pve01 10.0.0.61
add-host_to_servers imm-pve02 10.0.0.62
add-host_to_servers imm-pve03 10.0.0.63
add-host_to_servers imm-pve04 10.0.0.64
add-host_to_servers imm-pve05 10.0.0.65
add-host_to_servers imm-pve06 10.0.0.66

## Virtualization Server
add-host_to_servers pve01 10.0.0.71
add-host_to_servers pve02 10.0.0.72
add-host_to_servers pve03 10.0.0.73
add-host_to_servers pve04 10.0.0.74
add-host_to_servers pve05 10.0.0.75
add-host_to_servers pve06 10.0.0.76

## Backup Server
add-host_to_servers pbs01 10.0.0.81

## Clone Server
add-host_to_servers fog01 10.0.0.91
add-host_to_servers fog02 10.0.0.92

## Various Services
add-host_to_servers bbb01 10.0.0.171
add-host_to_servers esx01 10.0.0.172
add-host_to_servers immesx 10.0.0.173

## Uniflow print server
add-host_to_servers uniflow01 10.0.0.201

## Gateways
## Gateway to 83.175.116.5 (IKB)
add-host_to_servers nat02 10.0.0.253
##Gateway to 83.175.116.4 (IKB)
add-host_to_servers nat01 10.0.0.254

## Printer all Rooms
add-host_to_servers r001pr01 10.0.1.91
add-host_to_servers r002pr01 10.0.2.91
add-host_to_servers r003pr01 10.0.3.91
add-host_to_servers r004pr01 10.0.4.91
add-host_to_servers r005pr01 10.0.5.91
add-host_to_servers r007pr01 10.0.7.91
add-host_to_servers r007pr02 10.0.7.92
add-host_to_servers r008pr01 10.0.8.91

add-host_to_servers r107pr01 10.1.7.91
add-host_to_servers r107pr03 10.1.7.93

add-host_to_servers r115pr01 10.1.15.91
add-host_to_servers r115pr02 10.1.15.92

add-host_to_servers r207pr01 10.2.7.91
add-host_to_servers r216pr01 10.2.16.91

add-host_to_servers r307pr01 10.3.7.91
add-host_to_servers r314pr01 10.3.14.91

add-host_to_servers r099pr01 10.0.99.91
add-host_to_servers r099pr02 10.0.99.92
add-host_to_servers r099pr03 10.0.99.93

## virtual Room for Canon UNIFLOW - Printer
add-host_to_servers rcanon01 10.0.90.1
add-host_to_servers rcanon02 10.0.90.2
add-host_to_servers rcanon03 10.0.90.3

## Raspberry PIs for Timetable
## Raspi Aula
add-host_to_servers stupla01 10.1.9.1
## Raspi GfB
add-host_to_servers stupla02 10.1.12.1
