#!/bin/bash

# source helper functions
. ../tools/helperfunctions.sh

# source configuration
. ../SAMBA4.conf

## manipulated file /etc/samba/smb.conf
FILE=/etc/samba/smb.conf
printAndLogMessage "SET FORWARDER IN ${FILE}"
printAndLogMessage "Manipulated file: " ${FILE}
printAndLogMessage "Save original file: " ${FILE}
saveOriginal ${FILE}
logFile ${FILE}

printAndLogMessage "Change file: " ${FILE}

## HINTS to understand sed:
## 1: we have to use " instead ' because of ${...} variable substitution
## 2: remove all quoting \ in s/.../.../ and you get a readable regex
sed -e "/dns/ s/\([[:digit:]]\{1,3\}\.\)\{3\}[[:digit:]]\{1,3\}/${DNS_FORWARDER_IP_LIST}/" -i ${FILE}

logFile ${FILE}
