#!/bin/bash

# source helper functions
. ../tools/helperfunctions.sh

# source configuration
. ../SAMBA4.conf

## CREATE CONTAINER FOR MANAGER GROUPS/USERS
printAndLogMessage "CREATE CONTAINER FOR MANAGER GROUPS/USERS ${OU_MANAGER_CONTAINER}"
createSambaOUpath "${OU_MANAGER_CONTAINER}"

## CREATE CONTAINER FOR DOMAIN-COMPUTERS
printAndLogMessage "CREATE CONTAINER FOR DOMAIN COMPUTERS ${OU_DOMAIN_COMPUTERS_CONTAINER}"
createSambaOUpath "${OU_DOMAIN_COMPUTERS_CONTAINER}"

## CREATE CONTAINER FOR DOMAIN-SERVERS
printAndLogMessage "CREATE CONTAINER FOR DOMAIN SERVERS ${OU_DOMAIN_SERVERS_CONTAINER}"
createSambaOUpath "${OU_DOMAIN_SERVERS_CONTAINER}"

## CREATE COMPUTER-DOMAIN-JOIN-GROUP
printAndLogMessage "CREATE COMPUTER DOMAIN JOIN GROUP ${DOMAIN_JOIN_GROUP}"
samba-tool group add "${DOMAIN_JOIN_GROUP}"

## MOVE COMPUTER-DOMAIN-JOIN-GROUP TO MANAGER-CONTAINER
printAndLogMessage "move ${DOMAIN_JOIN_GROUP} to ${OU_MANAGER_CONTAINER}"
samba-tool group move "${DOMAIN_JOIN_GROUP}" "${OU_MANAGER_CONTAINER}"

printAndLogMessage "ADD ACCLS TO ${OU_DOMAIN_COMPUTERS_CONTAINER} FOR ${DOMAIN_JOIN_GROUP}"
## we get something like this:
## dn: CN=domain_join_group,OU=701036,DC=brg,DC=tsn
## objectSid: S-1-5-21-1101371487-3695870978-1069032044-1103
## we extract SID, which is last word of grep output.
OBJECT_SID=$(echo $(samba-tool group show "${DOMAIN_JOIN_GROUP}" --attributes=objectSid) \
              | grep "objectSid" \
              | awk '{print $NF}')
## we build SDDL String
SDDL="(${JOIN_ACCESS_FLAG};${JOIN_INHERIT_FLAG};${JOIN_ACL_LIST};;;${OBJECT_SID})"
printAndLogMessage "SDDL ${SDDL} added to object ${OU_DOMAIN_COMPUTERS_CONTAINER}"
## we need DN for ${OU_TSN_SYNC_CONTAINER} :-(
samba-tool dsacl set --objectdn="${OU_DOMAIN_COMPUTERS_CONTAINER},${SAMBA4_ROOT_DN}" --sddl="${SDDL}"

## CREATE DOMAIN-JOIN-USER
printAndLogMessage "CREATE DOMAIN-JOIN-USER ${DOMAIN_JOIN_USER}"
samba-tool user create "${DOMAIN_JOIN_USER}" "${DOMAIN_JOIN_USER_PASSWORD}"

## ADD DOMAIN-JOIN-USER TO COMPUTER-DOMAIN-JOIN-GROUP
printAndLogMessage "add ${DOMAIN_JOIN_USER} to ${DOMAIN_JOIN_GROUP}"
samba-tool group addmembers "${DOMAIN_JOIN_GROUP}" "${DOMAIN_JOIN_USER}"

## MOVE DOMAIN-JOIN-USER TO MANAGER-CONTAINER
printAndLogMessage "move ${DOMAIN_JOIN_USER} to ${OU_MANAGER_CONTAINER}"
samba-tool user move "${DOMAIN_JOIN_USER}" "${OU_MANAGER_CONTAINER}"
