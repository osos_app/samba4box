#!/bin/bash

# source helper functions
. ../tools/helperfunctions.sh

# source configuration
. ../SAMBA4.conf

if [[ "${USE_DOWNLOADED_CERTIFICATE}" == "no" ]]; then
  printAndLogMessage "no own tls/ssl certificate will be installed"
  exit
fi

## manipulated file /etc/samba/smb.conf
FILE=/etc/samba/smb.conf
if grep -q "tls enabled" ${FILE}; then
  printAndLogMessage "SSL/TLS already enabled, we do change /etc/samba/smb.conf!"
else
  printAndLogMessage "WRITE NEW ${FILE}"
  printAndLogMessage "Manipulated file: " ${FILE}

  printAndLogMessage "STOP samba-ad-dc"
  systemctl stop samba-ad-dc

  printAndLogMessage "Save original file: " ${FILE}
  saveOriginal ${FILE}
  logFile ${FILE}

  printAndLogMessage "Change file: " ${FILE}
  # ATTENTION: \ttls = tabstop \t and tls
  sed -e "/idmap_ldb*/a  \ \ttls enabled = yes \n \ttls keyfile = tls/${MY_KEY_PEM} \n \ttls certfile = tls/${MY_CERT_PEM} \n \ttls cafile  =" -i ${FILE}

  logFile ${FILE}

  printAndLogMessage "START samba-ad-dc"
  systemctl start samba-ad-dc
fi

## extract certifikate from pkcs12 file, which has to be placed in
## ad_server/tls directory

KEYSTORE_FILE=$(ls tls/*.p12)
if [[ "${KEYSTORE_FILE}" == "" ]]; then
  printAndLogMessage "no *.p12 container found, we exit!"
  exit
fi

PASSWORD_FILE="tls/password.txt"
if [[ "${PASSWORD_FILE}" == "" ]]; then
  printAndLogMessage "no pasword.txt file found, we exit!"
  exit
fi

printAndLogMessage "STOP samba-ad-dc"
systemctl stop samba-ad-dc

NEW_FILE="/var/lib/samba/private/tls/${MY_CERT_PEM}"
openssl pkcs12 -in "${KEYSTORE_FILE}" -out "${NEW_FILE}" -nodes -nokeys -passin file:"${PASSWORD_FILE}"

NEW_FILE="/var/lib/samba/private/tls/${MY_KEY_PEM}"
openssl pkcs12 -in "${KEYSTORE_FILE}" -out "${NEW_FILE}" -nodes -nocerts -passin file:"${PASSWORD_FILE}"

printAndLogMessage "START samba-ad-dc"
systemctl start samba-ad-dc
