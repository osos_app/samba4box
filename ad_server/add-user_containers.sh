#!/bin/bash

# source helper functions
. ../tools/helperfunctions.sh

# source configuration
. ../SAMBA4.conf

## CREATE OU_USER_CONTAINER_LIST
for CONTAINER in ${OU_USER_CONTAINER_LIST}; do
  OU_TO_CREATE="${CONTAINER}","${OU_TOPLEVEL}"

  ## CREATE TSN-SYNC-CONTAINER
  printAndLogMessage "CREATE USER-CONTAINER ${OU_TO_CREATE}"
  createSambaOUpath "${OU_TO_CREATE}"
done
