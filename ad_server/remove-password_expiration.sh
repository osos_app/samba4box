#!/bin/bash

# source helper functions
. ../tools/helperfunctions.sh

# source configuration
. ../SAMBA4.conf

## REMOVE PASSWORD-SETTING --max-pwd-age=42 (default) and stop password expiration
printAndLogMessage "STOP password expiration"
samba-tool domain passwordsettings show

samba-tool domain passwordsettings set --max-pwd-age=0

samba-tool domain passwordsettings show
