#!/bin/bash

SCHOOLNUMBER=$1

if [[ "${SCHOOLNUMBER}" == "" ]]; then
  echo "Please call script $0 with your school-number"
  echo "Example: $0 701036"
  exit
fi

BOOKMARK_LIST="file:///home/shares/lehrmaterial file:///home/shares/schueler file:///home/users/${SCHOOLNUMBER}/e/examinees"

HOME_DIR_BASE="/home/users/${SCHOOLNUMBER}/l"
cd ${HOME_DIR_BASE}
HOME_DIR_LIST=$(ls)

for USER in ${HOME_DIR_LIST};
do
	echo "###########################################################################"
	echo "# Checking GNOME Bookmarks for ${USER}"
	if [[ -f ${USER}/.config/gtk-3.0/selfmanaged.txt ]];
	then
		echo "## Self managed GNOME bookmarks for user: ${USER}, exit!"
		continue
	fi
	if [[ ! -f ${USER}/.config/gtk-3.0/bookmarks ]];
	then
		echo "## NO bookmarks file for user: ${USER}, exit!"
		continue
	fi
	for BOOKMARK in ${BOOKMARK_LIST};
	do
		echo "## Testing for bookmark: ${BOOKMARK}"
		if grep -q ${BOOKMARK} ${USER}/.config/gtk-3.0/bookmarks;
		then
			echo "### Found bookmark: ${BOOKMARK} for user: ${USER}, exit!"
		else
			echo "### Adding bookmark: ${BOOKMARK} for user: ${USER}"
			echo ${BOOKMARK} >> ${USER}/.config/gtk-3.0/bookmarks
		fi
	done
done
