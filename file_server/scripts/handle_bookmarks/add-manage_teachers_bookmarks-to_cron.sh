#!/bin/bash

SCHOOLNUMBER=$1

CRON_DAILY_SCRIPT_NAME="manage_teachers_bookmarks_${SCHOOLNUMBER}"

if [[ "${SCHOOLNUMBER}" == "" ]]; then
  echo "Please call script $0 with your school-number"
  echo "Example: $0 701036"
  exit
fi

echo "add handle bookmarks for ${SCHOOLNUMBER}"

## copy script 
cp files/manage_teachers_bookmarks.sh /usr/local/sbin

## ATTENTION: #!/bin/bash has to be IN 1. line
echo "#!/bin/bash

/bin/bash /usr/local/sbin/manage_teachers_bookmarks.sh ${SCHOOLNUMBER}
" >/etc/cron.daily/"${CRON_DAILY_SCRIPT_NAME}"

chmod a+x /etc/cron.daily/${CRON_DAILY_SCRIPT_NAME}
