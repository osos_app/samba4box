# Helper Scripts for Fileserver

## Sync for manual USE in directory Fileserver

sync_homedirs_for_ad_users will run out of the box in directory Fileserver

## Sync for manual USE from /usr/local/sbin

If you like to run sync_homedirs_for_ad_user from everywhere copy

* sync_homedirs_for_ad_user
* check-for_unused_homedirs.sh
* check-for_missing_homedirs.sh

to /usr/local/sbin

and change in file sync_homedirs_for_ad_user:

* ./check-for_unused_homedirs.sh -> /usr/local/sbin/check-for_unused_homedirs.sh
* ./check-for_missing_homedirs.sh to /usr/local/sbin/heck-for_missing_homedirs.sh

## Sync for USE in /etc/cron.daily

Do as described in Sync for manual USE from /usr/local/sbin but copy sync_homedirs_for_ad_user to /etc/cron.daily

