#!/bin/bash

SCHOOLNUMBER=$1

CRON_DAILY_SCRIPT_NAME="clean_exams_for_${SCHOOLNUMBER}"

if [[ "${SCHOOLNUMBER}" == "" ]]; then
  echo "Please call script $0 with your school-number"
  echo "Example: $0 701036"
  exit
fi

echo "add save_exams.sh for ${SCHOOLNUMBER}"

## create dir for saved old exams
mkdir -v "/home/users/${SCHOOLNUMBER}/e/zzzOLD"

## copy save script to dir for saved old exams
cp files/save_exams.sh "/home/users/${SCHOOLNUMBER}/e/zzzOLD"

## ATTENTION: #!/bin/bash has to be IN 1. line
echo "#!/bin/bash

/bin/bash /home/users/${SCHOOLNUMBER}/e/zzzOLD/save_exams.sh ${SCHOOLNUMBER}
" >/etc/cron.daily/"${CRON_DAILY_SCRIPT_NAME}"

chmod a+x /etc/cron.daily/${CRON_DAILY_SCRIPT_NAME}
