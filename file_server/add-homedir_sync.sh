#!/bin/bash

# source helper functions
. ../tools/helperfunctions.sh

# source configuration
. ../SAMBA4.conf

printAndLogMessage "copy helperscripts to /usr/local/sbin"
cp -v scripts/sync_home_dirs/check-for_missing_homedirs.sh /usr/local/sbin
cp -v scripts/sync_home_dirs/check-for_unused_homedirs.sh /usr/local/sbin
cp -v scripts/sync_home_dirs/sync_homedirs_for_ad_users /usr/local/sbin

printAndLogMessage "link homedir sync to /etc/cron.daily"
## ATTENTION: #!/bin/bash has to be IN 1. line
echo "#!/bin/bash

## check, if there are homedirs with NO user-accounts in AD
/bin/bash /usr/local/sbin/check-for_unused_homedirs.sh

## check, if there are user-accounts in AD with NO homedir
/bin/bash /usr/local/sbin/check-for_missing_homedirs.sh
" >/etc/cron.daily/sync_homedirs_for_ad_users
chmod -v a+x /etc/cron.daily/sync_homedirs_for_ad_users
