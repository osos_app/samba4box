# samba4box

Installation of a complete SAMBA4 Network, with 

- SAMBA4 ad-directory-server,

- directory-replication-server,

- SMB-files-server

- NFS4-file server with kerberos

- Linux-clients with NFS4 - home directories from NFS4-servers


Repository contains:

## Scripts for automatic setup of:

- **SAMBA 4 Activ - Directory - Server** with an connected

- **SAMBA File - Servers** which additional act as **krb5 NFS4 File - Server**

on a Ubuntu 20.04 or 22.04 Server


## Scripts to handle creation of AD-objects for:

- **users** from 
    - csv-file or
    - DVT Sync (only in Tirol/Austria :-( )

- **hosts** optional from 
    - csv-file or from 
    - dhcp-server


## Scripts to automate:

- **creation** = create home-directory if, it does not exist for an user

- **cleanup** = store old home-directory to stash, if directory for deleted user still exists

of user home-directories on the file - servers.


## Scripts to:

- connect Linux-clients to AD, with a non administrator account and

- setup NFS shares on these Linux - client


## Script to:

- save

- clean

written exams from examinee-users-directories in the included examinee enviroment


## The Example:

Possible the best way to explore, how all these things work together, is to set up the included example.

In this example, we will build a school cluster including 4 different schools, using the same infrastructure.

We had no problem to install the example with search domain *osos.at* in a 10.0.0.0/8,

parallel the productive AD with a different search domain.

### And here is an overview, what you will get:

- all the stuff will live in *OU=SCHOOL,DC=osos,dc=at*

- *OU=Managers,OU=SCHOOL*  
  contains join-group and join-user to join computers to domain

- *OU=Computers,OU=SCHOOL* contains all PCs joined by join-user.  
  For security reasons join-group has special rights ONLY here

- *OU=Printers,OU=SCHOOL* move your printers here.

- *OU=Servers,OU=SCHOOL* put your servers here.


- *OU=701036,OU=SCHOOL* container, where first school lives

- *OU=Users,OU=701036,OU=SCHOOL* container for users from first school

- *OU=l,OU=Users,OU=701036,OU=SCHOOL* l = Lehrer*innen german for teachers

- *OU=s,OU=Users,OU=701036,OU=SCHOOL* s = Schüler*innen german for pupils

- *OU=v,OU=Users,OU=701036,OU=SCHOOL* v = Verwaltung german for administration

- *OU=e,OU=Users,OU=701036,OU=SCHOOL* e = Examinees

- *OU=Groups,OU=701036,OU=SCHOOL* container for groups for first school

- *OU=701076,OU=SCHOOL* container, where second school lives

- *OU=301056,OU=SCHOOL* container, where third school lives

- *OU=405010,OU=SCHOOL* container, where fourth school lives


### and the overview, what you will need to build the example:

#### computers

At least you will need 3 different computers.

Best practice is to have machines, where snapshots are available.

- ubuntu server for the samba - active - directory (512MB RAM 8GB disk)

- ubuntu server for the samba - file -server (512MB RAM 8GB disk)

- ubuntu Client (2GB RAM 16GB disk)

and optional, to install replication and second fileserver

- 2 more servers with (512MB RAM 8GB disk)

and recommended:

- a Linux - Desktop - PC, which exists in the same network as the directory, 
  for the Apache Directory Studio to study and work with the directory.

#### network

The network search path is set to:

***osos.at***

and you will have 2 possibilities to set up the example network:

- 10.0.0.0/8 network:

  Standard: with very small changes, when you install the example

  Please use following ***hostnames*** and ***IPs***

  |  HOSTNAME  | IP           | ROLE                         | MUST/optional                      |
  |------------|:-------------|:-----------------------------|:-----------------------------------|
  | v-ad01     | 10.0.80.1    | Activ Directory Server       | MUST                               |
  | v-ad02     | 10.0.80.2    | Replication Server           | optional                           |
  | v-hole01   | 10.0.80.3    | File Server                  | MUST for all users, expect pupils  |
  | v-hosu01   | 10.0.80.4    | File Server                  | optional                           |
  | v-r080pc11 | 10.0.80.11   | Client                       | MUST                               |
  | v-r080pc12 | 10.0.80.12   | Client                       | optional                           |

If 10.0.80.nnn IPs do not fit for your network, you can change in

samba4box/scripts/csv/net10/school_servers.csv
samba4box/scripts/csv/net10/school_hosts.csv

- 192.168.1.0/24 network:

  Optional: with a little more  small changes, when you install the example

  Please use following ***hostnames*** and ***IPs***

  | HOSTNAME    | IP           | ROLE                         | MUST/optional                     |
  |:------------|:-------------|:-----------------------------|:----------------------------------|
  | v-ad01      | 192.168.1.11 | Activ Directory Server       | MUST                              |
  | v-ad02      | 192.168.1.12 | Replication Server           | optional                          |
  | v-hole01    | 192.168.1.13 | File Server                  | MUST for all users, expect pupils |
  | v-hosu01    | 192.168.1.14 | File Server                  | optional                          |
  | v-r080pc11  | 192.168.1.21 | Client                       | MUST                              |
  | v-r080pc12  | 192.168.1.22 | Client                       | optional                          |

If 192.168.nnn.nnn IPs do not fit for your network, you can change in

*ad_server/scripts/csv/net192/school_servers.csv*

*ad_server/scripts/csv/net192/school_hosts.csv*

HINT:

Example with 192.168.1.0/24 network will run in most home networks, 

if you use network - bridge as network-adapter in Virtualbox


## How to install Example:


### Samba 4 directory server:

In this example we use the 10.0.0.0/8 network settings.

1. On a fresh Ubuntu - server get repository with:

   > root@v-ad01> git clone https://gitlab.com/osos_app/samba4box

   and take a **snapshot** now, if possible!

2. In samba4box edit SAMBA.conf:

   Set IP for this server, which is this servers IP!

   > *THIS_SERVER_STATIC_IP="10.0.80.1"*

   or

   > *THIS_SERVER_STATIC_IP="192.168.1.11"*

   Set IP for AD server, which is the same here:

   > *SAMBA4_AD_DNS_STATIC_IP="10.0.80.1"*

   or

   > *SAMBA4_AD_DNS_STATIC_IP="192.168.1.11"*

   Set IP for DNS forwarding

   > *DNS_FORWARDER_IP_LIST="nnn.nnn.nnn.nnn  nnn.nnn.nnn.nnn"*


3. DNS settings and CSV files for 192.168.1.0:

   **Skip chapter 3., if you use then 10.0.0.0/xx network!**  
   even if, our own network is 10.0.0.0/24.
   It doesn't matter, if the created DNS - server will work for a 10.0.0.0/8 network.
   
   DNS - Settings:

   In file *SAMBA.conf* find section:

   SAMBA4 ACTIVE DOMAIN CONTROLLER: DNS

   and change following as shown:

   > *DNS_NETWORK="10.0.0.0"*  
   > *DNS_NETMASK="255.0.0.0"*

   to

   > *DNS_NETWORK="192.168.1.0"*  
   > *DNS_NETMASK="255.255.255.0"*

   to create the fitting DNS reverse zone.


3. The Administrator password is set to *Passw0rd*; you can change it, if you want.

4. Change into directory ad_server and install with:

   > root@v-ad01> ./install-ad_dc.sh

5. ATTENTION: After installation

   check */etc/resolv.conf* with

   > root@v-ad01>  cat /etc/resolv.conf

   and it has to look like this:

   **nameserver 10.0.80.1** or **nameserver 192.168.1.11**

   **search osos.at**

   beause active - directory - server has to have itself as DNS - server!


### Apache Directory Studio:

For working with the directory we use Apache Directory Studio.

1. Download Apache Directory Studio from https://directory.apache.org/studio/downloads.html

2. Create new connection and set

   for Network parameter:

   - hostname: 10.0.80.1 or 192.168.1.11

   - port: 636

   - time: 30

   - Encrytion method: SSL/ldaps

   and for Authentification:

   - Authentification Method: Simple Authentification

   - Bind DN : *cn=Administrator,cn=users,dc=osos,dc=at*

   - Bind password: Passw0rd

Now you can have a look at the directory.



### Fill directory with example staff:

1. Fix CSV files - path for 192.168.1.0/24 network:

   **Skip chapter 1., if you use then 10.0.0.0/xx network!**

   In file *samba4box/ad_server/scripts/create-example_hosts.sh*

   change

   > NET_DIRECTORY="net_10"  
   > #NET_DIRECTORY="net_192"

   to 

   > #NET_DIRECTORY="net_10"  
   > NET_DIRECTORY="net_192"

2. Add example stuff to directory:

   On the directory server change into directory *samba4box/ad_server/scripts* and  
   create example with script

   > root@v-ad01> ./create_big_example_school.sh

   In Apache Directory Studio you can track changes tipping F5.

   Remember, you have to do this step before installing the fileserver, 
   because file-server objects with nfs-principals are create here!


### Samba 4 file server:

1. On a fresh Ubuntu - server with hostname ***v-hole01*** (**ho**me-**le**hrer) or ***v-hosu01*** (**ho**me-**s**ch**u**eler)
   again get repository with:

   > root@v-hole01> git clone https://gitlab.com/osos_app/samba4box

   and again take a **snapshot** now, if possible!

   IMPORTANT:

   In this example we use file server-names *v-hole01*, *v-hosu01*.
   Please stay on it, because client NFS - mount-points depend on it.

   For a possible later created productive environment, everything can be changed!


2. In samba4box edit SAMBA.conf:

   Set IP for this server!

   > *THIS_SERVER_STATIC_IP="10.0.80.3"*

   or

   > *THIS_SERVER_STATIC_IP="192.168.1.13"*

   Set IP for AD server, which is DIFFERENT from file servers own IP:

   > *SAMBA4_AD_DNS_STATIC_IP="10.0.80.1"*

   or

   > *SAMBA4_AD_DNS_STATIC_IP="192.168.1.11"*

3. Set range for NFS - Server:

   **Skip chapter 3., if you use then 10.0.0.0/xx network!**

   In section SAMBA4 NFS SERVER FOR HOMES

   change

   > NFS_ALLOWED_IP_RANGE="10.0.0.0/8"

   to 

   > NFS_ALLOWED_IP_RANGE="192.168.1.0/24"

   to fit NFS export to 192.168.1.0/24 network.

4. The Administrator password is set to *Passw0rd*, you MUST change it, if you did before.

5. Before installation check:

   - Timezone:

     if file server and ad - server are configured with the same timezone

   - DNS - Name-resolution:

     Your AD - server with its search domain has to be the working nameserver.

     Check:

     > root@v-hole01> host "your-ad-server-name"

     also */etc/resolv.conf* has to have same settings as on our ad - server

     You will avoid a lot of pain :-)


6. Change into directory file_server and install with:

   > root@v-hole01> ./install-ad_fileserver.sh


7. Check if installation & connection is ok:

   > root@v-hole01> wbinfo -u

   should show all users from ad - server


### Create user home-directorys on file server:

If you do not want to wait until cron.daily is creating all needed homedirs, type as root

> root@v-hole01> run-parts -v /etc/cron.daily

and homedirs for all users are crated via script

*scripts/sync_home_dirs/sync_homedirs_for_ad_users*

or in future situations moved to *zzzOLD*, if users do not exist anymore in ad - directory.


### Finish examinee environment

Examinees home-directoies are stored on file-server *v-hole01*.

We have to fix 2 things, which is done from script below.

- spezial rights for teachers on examinee_nnnnnn_nn to control, if exams are stored

- install and link *save_exams.sh* script to /etc/cron.daily/ to

  - save written exams

  - clean examinees delivery directory

So we call script now:

> root@v-hole01> ./finish-examinees_for_big_school.sh

in directory *scripts/handle_examinees/*.


### Linux Client:

1. On a fresh Ubuntu - Desktop with hostname ***v-r080pc11*** 
   again get repository with:

   > root@v-r080pc11> git clone https://gitlab.com/osos_app/samba4box

   and again take a **snapshot** now, if possible!

2. Before installation check:

   - Timezone:

   - DNS - Nameresolution:

     Your AD - server with its search domain has to be the working nameserver.

     Check:

     > root@v-r080pc11> host "your-ad-server-name"

     also */etc/resolv.conf* has to have same settings as on our servers


3. Set Parameters, different to example:

   * If you changed to 192.168.1.0/24 network you have to adjust the iptaple rule,  
     which is blocking the internet for examinees in:  
     *client/files/blockWWWForExaminees.sh*  
     > /sbin/iptables -I OUTPUT -d 10.0.0.0/8 -j ACCEPT  
     to  
     > /sbin/iptables -I OUTPUT -d 192.168.1.0/24 -j ACCEPT  

   * If you changed domain name and NFS - mount-points, please have a look at chapter *From example to production/Client*


4. Change into directory *client* and install with:

   > root@v-r080pc11> ./install-ad_client.sh

5. Check if installation & connection is ok:

   > root@v-hole01> wbinfo -u

   should show all users from ad - server
   
6. Please restart, and afterwards everything should work :-)

   You will find all possible users in the directory or in files *ad_server/scripts/csv/school_nnnnnn_users.csv*

   All passwords are set to "Passw0rd"

   ATTENTION:

   For all pupils the home-directories are located on file server *v-husu01*, which was not created until now.

   So you should not login as pupil now.

   But you can create *v-hosu01* with IP 10.0.80.4 or 192.168.1.14 the same way as *v-hole01*, to complete the example.

## From example to production:

### Directory Server:

First you have to understand, that structure in the directory is just for use.

A client will find a user or a host everywhere. There are no restrictions set here.

So you can rename and move OUs as you like :-)


### Home-directories:

Administration pain start with users - home-directories.

* File servers have to know what to serve.

* Clients have to know what to import from where.

* SMB shares have to lay in */home/users* as a standard.

  So Windows *homeDirectory* is set to *\\v-hole01.app.tsn\users\701076\l\teacher_701076_01*

  **homeDirectory: \\\v-hole01.app.tsn\users\701076\l\teacher_701076_01**

  This points to */home/users/701076/l/teacher_701076_01* on the file-server.  
  it makes sense to set unix home-directory as follows:

  **unixHomeDirectory: /home/users/701076/l/teacher_701076_01**

  and create mountpoints on the client, which will be the same.

  So Windows homedrive *H:* and Linux *HOME* will be the same directory on the file-server for every user.

* For reasons of flexibility we decided to create NFS home-directoiy - exports

  and for every OU in 

  > OU_USER_CONTAINER_LIST="OU=701036 OU=701076 OU=301056 OU=405016"

  and for every group in 

  > GROUP_IDENTIFIER_LIST="l s v e"

  both set in SAMBA.conf. 

  So we created 4x4 = 16 NFS exports for our big example :-)

  To get a feeling, have a look at 

  > **/etc/fstab** for the bind - mounts and 

  > **/etc/exports** for exports

  on your filee-server(s).

* If you don't like this double structure, you can do as follows:

  - design your own exports.

  - install with example structure as dummy structure

  - change entries in */etc/fstab* and */etc/exports* to fit your wishes :-)


### Client:

#### General:

First you should know, that client - installation, does NOT depend on SAMBA.conf.

So these scripts can be used in different ways to perform same form of automatic installation.

If you do not have your own way, you can find our minimalistic system here:

[https://gitlab.com/osos_app/autoinstall](https://gitlab.com/osos_app/autoinstall)

It depends on 2 scripts, to run a lot of other scripts installing and configuring hosts.

Bringing up the infrastructure via tftp for booting via net and installing the first hooks to our *autoinstall* can be found here:

[https://gitlab.com/osos_app/networkbox/-/tree/main/tftpboot](https://gitlab.com/osos_app/networkbox/-/tree/main/tftpboot)

and the associated laus-server can be found here:

[https://gitlab.com/osos_app/networkbox/-/tree/main/laus](https://gitlab.com/osos_app/networkbox/-/tree/main/laus)

There are 2 things to do, to transform a Linux-client to a useful AD-member.

#### AD - client:

We have to join the domain.

There are some "modern" like SSSD, but we could not bringt SSSD & NFS (krb5) to work together.

So we stayed on winbind, which isn't as bad, because file-servers and clients use the same */etc/samba/smb.conf*. Nice :-)

Settings which belong to domain - join, has to be set in:

*samba4box/client/add-ad_client.sh*

SAMBA4_DNS_DOMAIN_NAME="osos.at"  
SAMBA4_REALM_DOMAIN_NAME="OSOS.AT"  
SAMBA4_DOMAIN="OSOS"  
DOMAIN_JOIN_USER="domain_join_user"  
DOMAIN_JOIN_USER_PASSWORD="Passw0rd"

#### NFS - client:

Because we not only wanted to be a domain member, but also have our home directories served from file servers,

we have to tell our client the names of all the friendly file servers.

And at least the client itself has to know where all the directories from "outside" shall be shown on the client.

All these settings are done in:

*samba4box/client/add-home_nfs_shares.sh*

FS_HOME_TEACHERS_701036="v-hole01"  
FS_HOME_PUPILS_701036="v-hosu01"  
FS_HOME_V_701036="v-hole01"  
FS_HOME_EXAMINEES_701036="v-hole01"

and we follow the structure form file server exports.

If you want changes fitting for your network, you have to do some rewriting here, to change the mountpoints.

But changes here are straight forward. Just rename mount-points and corresponding entries for 

*/etc/auto.master*

and 

*/etc/auto."what_you_like"


