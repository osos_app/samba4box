#!/bin/bash

# source helper functions
. ../tools/helperfunctions.sh

# source configuration
. ../SAMBA4.conf

# install samba4 ad controller
printAndLogStartMessage "START: INSTALLATION OF SAMBA4 AD - CONTROLLER"
export DEBIAN_FRONTEND=noninteractive
apt-get -y update

printAndLogStartMessage "CHECK /etc/hosts FOR FQDN & STATIC IP"
## check if FQDN exists in /etc/hosts
if [[ "$(hostname)" = "$(hostname --fqdn)" ]]; then
  /bin/bash ../tools/change-FQHN-etc_hosts.sh
fi
## set the static IP in /etc/hosts
/bin/bash ../tools/change-IP-etc_hosts.sh

printAndLogMessage "INSTALL PACKAGES"
printAndLogMessage "apt-get install -y acl attr samba samba-dsdb-modules samba-vfs-modules winbind krb5-config krb5-user bind9-dnsutils ldb-tools adcli nfs-common autofs net-tools"
## from samba - wiki: acl attr samba samba-dsdb-modules samba-vfs-modules winbind krb5-config krb5-user
## acl, attr: extended acls
## bind9-dnsutils: dig, nslookup
## to have a look at the ldb-databases: ldb-tools
## to preset computer-accounts: adcli
## to act as an nfs-client: nfs-common autofs
## for testing: net-tools
apt-get install -y acl attr samba samba-dsdb-modules samba-vfs-modules winbind krb5-config krb5-user bind9-dnsutils ldb-tools adcli net-tools

printAndLogMessage "MASK, DISABLE & STOP smbd nmbd winbind"
systemctl mask smbd nmbd winbind
systemctl disable smbd nmbd winbind
systemctl stop smbd nmbd winbind

printAndLogMessage "CLEAN SAMBA & KERBEROS CONFIGURATION FILES"
printAndLogMessage "mv /etc/samba/smb.conf /etc/samba/smb.conf.orig"
printAndLogMessage "mv /etc/krb5.conf /etc/krb5.conf.orig"
mv /etc/samba/smb.conf /etc/samba/smb.conf.orig
mv /etc/krb5.conf /etc/krb5.conf.orig

## SET TIMEZONE to ${SAMBA4_TIMEZONE}
printAndLogMessage "SET TIMEZONE TO ${SAMBA4_TIMEZONE}"
echo "### current time:" "$(date)"
# timedatectl set-timezone ${SAMBA4_TIMEZONE} does NOT work in 22.04
# so we fall back to "easy" solution :-(
ln -fs /usr/share/zoneinfo/"${SAMBA4_TIMEZONE}" /etc/localtime
echo "### new time:" "$(date)"

printAndLogMessage "REPLICATE ACTIVE DIRECTORY SERVER"

samba-tool domain join "${SAMBA4_REALM_DOMAIN_NAME}" DC --option='idmap_ldb:use rfc2307 = yes' -U"${SAMBA4_DOMAIN}\administrator"

printAndLogMessage "INSTALL KERBEROS CONFIGURATION FROM SAMBA4"
printAndLogMessage "cp /var/lib/samba/private/krb5.conf /etc"
cp /var/lib/samba/private/krb5.conf /etc

## STOP AND DISABLE systemd-resolved &
## SET OWN IP AS NAMESERVER IN NEW $file"
## has to be done AFTER
## * apt-get install
## * samba-tool domain provision
## because DNS lookup is broken
## ATTENTION:
## We set IP at the moment in PROXMOX - GUI
/bin/bash ../tools/change-etc_resolv.conf.sh

printAndLogMessage "UMASK, START & ENABLE samba-ad-dc"
systemctl unmask samba-ad-dc
systemctl start samba-ad-dc
systemctl enable samba-ad-dc

## ADD CERTIFICATES TO DOMAIN CONTROLLER
#printAndLogMessage  "ADD CERTIFICATES TO DOMAIN CONTROLLER"
#/bin/bash add_tls-etc_samba_smb.conf.sh

## SIMPLE NETLOGON.BAT TO /var/lib/samba/sysvol/${SAMBA4_DNS_DOMAIN_NAME}/scripts
printAndLogMessage "copy simple netlogon.bat to /var/lib/samba/sysvol/${SAMBA4_DNS_DOMAIN_NAME}/scripts"
cp ../ad_server/files/netlogon.bat /var/lib/samba/sysvol/"${SAMBA4_DNS_DOMAIN_NAME}"/scripts

## RESTART samba4
printAndLogMessage "RESTART samba-ad-dc"
systemctl restart samba-ad-dc

printAndLogEndMessage "FINISH: INSTALLATION OF SAMBA4 AD - CONTROLLER"
