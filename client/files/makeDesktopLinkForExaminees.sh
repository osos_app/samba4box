#!/bin/bash

if [[ ${USER} == examinee* ]]; then
  ## we extract SCHOOLNUMBER from username examinee_701036_001
  SCHOOL_NUMBER=${USER:9:6}
  if [[ ! -d "${HOME}/Abgabe_${USER}" ]]; then
    ln -s "/home/users/${SCHOOL_NUMBER}/e/examinees/${USER}" "${HOME}/Abgabe_${USER}"
  fi

  if [[ ! -d "${HOME}/Abgabe_Vorlagen" ]]; then
    ln -s "/home/users/${SCHOOL_NUMBER}/e/examinees/Vorlagen/" "${HOME}/Abgabe_Vorlagen"
  fi

  if [[ ! -d "${HOME}/Schreibtisch/Abgabe_${USER}" ]]; then
    ln -s "/home/users/${SCHOOL_NUMBER}/e/examinees/${USER}" "${HOME}/Schreibtisch/Abgabe_${USER}"
  fi

  if [[ ! -d "${HOME}/Schreibtisch/Vorlagen" ]]; then
    ln -s "/home/users/${SCHOOL_NUMBER}/e/examinees/Vorlagen/" "${HOME}/Schreibtisch/Vorlagen"
  fi
fi
