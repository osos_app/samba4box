#!/bin/bash

# source helper functions
. ../tools/helperfunctions.sh

## original from:
## https://gitlab.com/osos_app/autoinstall/laus/scritsForClasses/CONFIG_U2004_AD/220-addHomeNFSShares.sh

## Fileservers for HOME-directories
FS_HOME_TEACHERS_701036="v-hole01"
FS_HOME_PUPILS_701036="v-hosu01"
FS_HOME_V_701036="v-hole01"
FS_HOME_EXAMINEES_701036="v-hole01"

FS_HOME_TEACHERS_701076="v-hole01"
FS_HOME_PUPILS_701076="v-hosu01"
FS_HOME_V_701076="v-hole01"
FS_HOME_EXAMINEES_701076="v-hole01"

FS_HOME_TEACHERS_301056="v-hole01"
FS_HOME_PUPILS_301056="v-hosu01"
FS_HOME_V_301056="v-hole01"
FS_HOME_EXAMINEES_301056="v-hole01"

FS_HOME_TEACHERS_405016="v-hole01"
FS_HOME_PUPILS_405016="v-hosu01"
FS_HOME_V_405016="v-hole01"
FS_HOME_EXAMINEES_405016="v-hole01"


## Configuration autofs
file="/etc/auto.master"
printAndLogMessage "Save original file: " ${file}
saveOriginal ${file}
logFile ${file}

## append path of config files
## for auto mountpoints to /etc/auto.master
echo "
#
# HOME - directories für teachers BRG
/home/users/701036/l    /etc/auto.users.701036.l        --ghost
#
# HOME - directories für teachers GfB
/home/users/701076/l    /etc/auto.users.701076.l        --ghost
#
# HOME - directories für pupils BRG
/home/users/701036/s    /etc/auto.users.701036.s        --ghost
#
# HOME - directories für pupils GfB
/home/users/701076/s    /etc/auto.users.701076.s        --ghost
#
# HOME - directories für Verwaltung BRG
/home/users/701036/v    /etc/auto.users.701036.v        --ghost
#
# HOME - directories für Verwaltung GfB
/home/users/701076/v    /etc/auto.users.701076.v        --ghost
#
# RETURN - directories für Examinees BRG
/home/users/701036/e    /etc/auto.users.701036.e        --ghost
#
# RETURN - directories für Examinees GfB
/home/users/701076/e    /etc/auto.users.701076.e        --ghost
#
#
################################################################
#
# HOME - directories für teachers 301056
/home/users/301056/l    /etc/auto.users.301056.l        --ghost
#
# HOME - directories für teachers 405016
/home/users/405016/l    /etc/auto.users.405016.l        --ghost
#
# HOME - directories für pupils 301056
/home/users/301056/s    /etc/auto.users.301056.s        --ghost
#
# HOME - directories für pupils 405016
/home/users/405016/s    /etc/auto.users.405016.s        --ghost
#
# HOME - directories für Verwaltung 301056
/home/users/301056/v    /etc/auto.users.301056.v        --ghost
#
# HOME - directories für Verwaltung 405016
/home/users/405016/v    /etc/auto.users.405016.v        --ghost
#
# RETURN - directories für Examinees 301056
/home/users/301056/e    /etc/auto.users.301056.e        --ghost
#
# RETURN - directories für Examinees 405016
/home/users/405016/e    /etc/auto.users.405016.e        --ghost
#
################################################################
" >> /etc/auto.master

logFile ${file}


## sec = krb5 will be taken from NFS - Server
## that meens: if NFS - server exports with sec=krb5
## client will mount with sec=krb5
## if NFS - server exports without sec=krb5
## client will mount without sec=krb5
##
## create config file for teachers BRG
echo "
*	-fstype=nfs4		${FS_HOME_TEACHERS_701036}:/701036/l/&
" > /etc/auto.users.701036.l

## create config file for teachers GfB
echo "
*	-fstype=nfs4		${FS_HOME_TEACHERS_701076}:/701076/l/&
" > /etc/auto.users.701076.l

## create config file for Verwaltung BRG
echo "
*	-fstype=nfs4		${FS_HOME_V_701036}:/701036/v/&
" > /etc/auto.users.701036.v

## create config file for Verwaltung GfB
echo "
*	-fstype=nfs4		${FS_HOME_V_701076}:/701076/v/&
" > /etc/auto.users.701076.v

## create config file for pupils BRG
echo "
*	-fstype=nfs4    	${FS_HOME_PUPILS_701036}:/701036/s/&
" > /etc/auto.users.701036.s

## create config file for pupils GfB
echo "
*	-fstype=nfs4    	${FS_HOME_PUPILS_701036}:/701076/s/&
" > /etc/auto.users.701076.s

## Examinees 
## we can NOT use wildcard, because teachers have to see all examinees
## for controlling and collecting

## create config file for Examinees BRG
echo "
examinees	-fstype=nfs4		${FS_HOME_EXAMINEES_701036}:/701036/e
" > /etc/auto.users.701036.e

## create config file for Examinees GfB
echo "
examinees	-fstype=nfs4		${FS_HOME_EXAMINEES_701076}:/701076/e
" > /etc/auto.users.701076.e

###########################################################################
###########################################################################
## create config file for teachers 301056
echo "
*	-fstype=nfs4		${FS_HOME_TEACHERS_301056}:/301056/l/&
" > /etc/auto.users.301056.l

## create config file for teachers 405016
echo "
*	-fstype=nfs4		${FS_HOME_TEACHERS_405016}:/405016/l/&
" > /etc/auto.users.405016.l

## create config file for Verwaltung 301056
echo "
*	-fstype=nfs4		${FS_HOME_V_301056}:/301056/v/&
" > /etc/auto.users.301056.v

## create config file for Verwaltung 405016
echo "
*	-fstype=nfs4		${FS_HOME_V_405016}:/405016/v/&
" > /etc/auto.users.405016.v

## create config file for pupils 301056
echo "
*	-fstype=nfs4    	${FS_HOME_PUPILS_301056}:/301056/s/&
" > /etc/auto.users.301056.s

## create config file for pupils 405016
echo "
*	-fstype=nfs4    	${FS_HOME_PUPILS_405016}:/405016/s/&
" > /etc/auto.users.405016.s

## Examinees 
## we can NOT use wildcard, because teachers have to see all examinees
## for controlling and collecting

## create config file for Examinees 301056
echo "
examinees	-fstype=nfs4		${FS_HOME_EXAMINEES_301056}:/301056/e
" > /etc/auto.users.301056.e

## create config file for Examinees 405016
echo "
examinees	-fstype=nfs4		${FS_HOME_EXAMINEES_405016}:/405016/e
" > /etc/auto.users.405016.e

systemctl restart autofs
