#!/bin/bash

# source helper functions
. ../tools/helperfunctions.sh

printAndLogStartMessage "START: INSTALLATION OF AD - CLIENT"

export DEBIAN_FRONTEND=noninteractive
apt-get -y update

printAndLogMessage "INSTALL PACKAGES AD - CLIENT"
## INSTALL PACKAGES
## from samba - wiki: acl attr samba samba-dsdb-modules samba-vfs-modules winbind krb5-config krb5-user
## acl, attr: exteded acls
## bind9-dnsutils: dig, nslookup
## to get Domain Users/Groups onto Fileserver to set Directory/File Permissions: libnss-winbind
## to enable local logins for Domain Users: libpam-winbind
apt-get -y install acl attr samba samba-dsdb-modules samba-vfs-modules winbind krb5-config krb5-user bind9-dnsutils libnss-winbind libpam-winbind 

printAndLogMessage "INSTALL PACKAGES NFS - CLIENT"
## INSTALL PACKAGES
## nfs-commoun: for nfs -client
## autofs: automounter for nfs - shares
apt-get -y install nfs-common autofs

printAndLogMessage "INSTALL AD CLIENT"
/bin/bash add-ad_client.sh

printAndLogMessage "INSTALL NFS - KRB5 - CLIENT"
/bin/bash add-nfs_krb5_client.sh

printAndLogMessage "INSTALL NFS - HOME - SHARES"
/bin/bash add-home_nfs_shares.sh

printAndLogMessage "RESTART NFS"
systemctl restart nfs-client.target

printAndLogMessage "BLOCK WWW FOR EXAMINEES"
/bin/bash block-www_for_examinees.sh

printAndLogMessage "ADD DESKTOP LINK FOR EXAMINEES"
/bin/bash add-desktop_link_for_examinees.sh



printAndLogEndMessage "FINISH: INSTALLATION OF AD - CLIENT"


