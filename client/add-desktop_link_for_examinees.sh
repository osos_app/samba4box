#!/bin/bash

# create 
# desktop-file : /etc/xdg/autostart/app-makeDesktopLinkForExaminees.desktop
# with link to 
# shell-script : /usr/local/bin/makeDesktopLinkForExaminees.sh
echo "[Desktop Entry]
Version=1.0
Type=Application
Name=Examinee Desktop Links
Name[de]=Skript zum Erstellen der Desktop Links für Examinee Benutzer
Exec=/usr/local/bin/makeDesktopLinkForExaminees.sh
Terminal=false
X-GNOME-Autostart-Phase=Applications
NoDisplay=true
" >> /etc/xdg/autostart/app-makeDesktopLinkForExaminees.desktop

# copy shell script to: /usr/local/bin/makeDesktopLinkForExaminees.sh
cp files/makeDesktopLinkForExaminees.sh /usr/local/bin/makeDesktopLinkForExaminees.sh

chmod a+x /usr/local/bin/makeDesktopLinkForExaminees.sh

