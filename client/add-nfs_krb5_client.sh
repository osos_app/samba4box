#!/bin/bash

# source helper functions
. ../tools/helperfunctions.sh

## original from:
## https://gitlab.com/osos_app/autoinstall/laus/scritsForClasses/CONFIG_U2004_AD/410-createNFSkrb5Client.sh


# manipulated file
file="/etc/default/nfs-common"
printAndLogMessage "Save original file: " ${file}
saveOriginal ${file}
logFile ${file}

# single quotes because double qutes inside !
sed -e '{
	/NEED_GSSD=$/ s/NEED_GSSD=/NEED_GSSD="yes"/
}' -i $file

logFile ${file}

systemctl restart autofs
