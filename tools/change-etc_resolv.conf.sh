#!/bin/bash

# source helper functions
. ../tools/helperfunctions.sh

# source configuration
. ../SAMBA4.conf

printAndLogMessage "STOP AND DISABLE systemd-resolved"
## manipulated file /etc/resolv.conf
FILE=/etc/resolv.conf
printAndLogMessage "Manipulated file: " ${FILE}
printAndLogMessage "Save original file: " ${FILE}
saveOriginal ${FILE}
logFile ${FILE}

systemctl stop systemd-resolved
systemctl disable systemd-resolved
rm ${FILE}

printAndLogMessage "SET AD - DC IP AS NAMESERVER IN NEW ${FILE}"
echo "nameserver ${SAMBA4_AD_DNS_STATIC_IP}
search ${SAMBA4_DNS_DOMAIN_NAME}
" >${FILE}
logFile ${FILE}

printAndLogMessage "RESTART NETPLAN"
netplan apply
