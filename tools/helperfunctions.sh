#!/bin/bash

#######################################
# save file with "filename" to
# * "filename.history.$(date +%Y%m%d-%N)
# * "filename.original"
# Arguments:
#   filename
#######################################
function saveOriginal() {
  cp "${1}" "${1}".history."$(date +%Y%m%d-%N)"
  if ! [[ -f "${1}".original ]]; then
    cp "${1}" "${1}".original
  fi
}

#######################################
# restore file with "filename" from
# "filename.original"
# Arguments:
#   filename
#######################################
function restoreOriginal() {
  if [[ -f "${1}".original ]]; then
    rm "${1}"
    rm "${1}".history.*
    cp "${1}".original "${1}"
  fi
}

#######################################
# calculate reverse net and netmask
# 10.0.0.1/255.0.0.0 -> 1.0.0/10
# 192.168.1.1/255.255.255.0 -> 1/1.168.192
# Arguments:
#   1: IP ~ 10.0.0.1
#   2: NETMASK ~ 255.0.0.0 ...
# Exports:
#   * REVERSE_NET
#   * REVERSE_IP
#######################################
function getReverseNETAndIP() {
  _IP="${1}"
  _NETMASK="${2}"
  if [[ "${_NETMASK}" == "255.255.255.0" ]]; then
    REVERSE_NET=$(echo "${_IP}" | awk 'BEGIN { FS = "." } { print $3"."$2"."$1 }')
    REVERSE_IP=$(echo "${_IP}" | awk 'BEGIN { FS = "." } { print $4 }')
  elif [[ "${_NETMASK}" == "255.255.0.0" ]]; then
    REVERSE_NET=$(echo "${_IP}" | awk 'BEGIN { FS = "." } { print $2"."$1 }')
    REVERSE_IP=$(echo "${_IP}" | awk 'BEGIN { FS = "." } { print $4"."$3 }')
  elif [[ "${_NETMASK}" == "255.0.0.0" ]]; then
    REVERSE_NET=$(echo "${_IP}" | awk 'BEGIN { FS = "." } { print $1 }')
    REVERSE_IP=$(echo "${_IP}" | awk 'BEGIN { FS = "." } { print $4"."$3"."$2 }')
  fi
  export REVERSE_NET
  export REVERSE_IP
}

#######################################
# calculate CIDR subnetmask
# 255.0.0.0 -> 8
# 255.255.255.0 -> 24
# Arguments:
#   NETMASK ~ 255.0.0.0 ...
# Exports:
#   CIDR_SUBNETMASK
#######################################
function getCIDRsubnetmask() {
  _NETMASK=$1
  if [[ "${_NETMASK}" == "255.255.255.0" ]]; then
    CIDR_SUBNETMASK=24
  elif [[ "${_NETMASK}" == "255.255.0.0" ]]; then
    CIDR_SUBNETMASK=16
  elif [[ "${_NETMASK}" == "255.0.0.0" ]]; then
    CIDR_SUBNETMASK=8
  fi
  export CIDR_SUBNETMASK
}

#######################################
# create a OU in a SAMBA4 directory
# even when parent directory does NOT
# exist.
# OU=1a,OU=Users,OU=701036,OU=school,dc=osos,dc=at
# in existing empty
# OU=school,dc=osos,dc=at
# Arguments:
#   OU - path
# Exports:
#   None
#######################################
function createSambaOUpath() {
  if [[ "$1" == "" ]]; then
    echo "no OU set! Nothing to do! Exit!"
    exit
  fi
  ## set separator to ','
  IFS=','
  ## get OU as array
  INPUT_OU_ARRAY=($1)
  unset IFS
  ## array.length
  INPUT_OU_ARRAY_LENGTH=${#INPUT_OU_ARRAY[@]}
  echo "processing ${INPUT_OU_ARRAY_LENGTH} OUs"
  for ((start_of_OUs = INPUT_OU_ARRAY_LENGTH - 1; start_of_OUs >= 0; start_of_OUs--)); do
    OU_TO_CREATE=""
    for ((ou = INPUT_OU_ARRAY_LENGTH - 1; ou >= start_of_OUs; ou--)); do
      OU_TO_CREATE="${INPUT_OU_ARRAY[ou]},${OU_TO_CREATE}"
    done
    ## we check, if OU already exists
    if [[ "$(samba-tool ou list | grep ${OU_TO_CREATE::-1})" == "" ]]; then
      ## :: -1 we remove the last "," at the end of the string
      echo "creating ${OU_TO_CREATE::-1}"
      samba-tool ou create "${OU_TO_CREATE::-1}"
    fi
  done
}

LOGFILE="../protocol.log"

#######################################
# Echos all command line arguments as
# a "START message" with #####s to
# * console
# * LOGFILE
# Arguments:
#   all arguments after
#   printAndLogStartMessage arg1 arg2 ...
#######################################
function printAndLogStartMessage() {
  echo ""
  echo "#####################################################################"
  echo "#####  " "$@"
  echo "#####################################################################"
  echo ""
  {
    echo ""                                                                      # >>"${LOGFILE}"
    echo "#####################################################################" # >>"${LOGFILE}"
    echo "##### " "$@"                                                           # >>"${LOGFILE}"
    echo "#####################################################################" # >>"${LOGFILE}"
    echo ""                                                                      # >>"${LOGFILE}"
  } >>"${LOGFILE}"
}

#######################################
# Echos all command line arguments as
# * console
# * LOGFILE
# Arguments:
#   all arguments after
#   printAndLogStartMessage arg1 arg2 ...
#######################################
function printAndLogMessage() {
  echo "#####  " "$@"
  echo "$@" >>"${LOGFILE}"
}

#######################################
# Echos all command line arguments as
# a "END message" with #####s to
# * console
# * LOGFILE
# Arguments:
#   all arguments after
#   printAndLogStartMessage arg1 arg2 ...
#######################################
function printAndLogEndMessage() {
  echo ""
  echo "#####  " "$@"
  echo "#####################################################################"
  echo ""

  {
    echo ""                                                                      # >>"${LOGFILE}"
    echo "##### " "$@"                                                           # >>"${LOGFILE}"
    echo "#####################################################################" # >>"${LOGFILE}"
    echo ""                                                                      # >>"${LOGFILE}"
  } >>"${LOGFILE}"
}

#######################################
# Echos a file
# * console
# * LOGFILE
# Arguments:
#   filename
#######################################
function logFile() {
  echo "#####################################################################"
  echo "FILE: " "${1}"
  echo ""
  cat "${1}"
  echo ""
  echo "#####################################################################"

  {
    echo "#####################################################################" # >>$LOGFILE
    echo "FILE: " "${1}"                                                         # >>$LOGFILE
    echo ""                                                                      # >>$LOGFILE
    cat "${1}"                                                                   # >>$LOGFILE
    echo ""                                                                      # >>$LOGFILE
    echo "#####################################################################" # >>$LOGFILE
  } >>"${LOGFILE}"
}
