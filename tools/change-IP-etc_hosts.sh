#!/bin/bash

# source helper functions
. ../tools/helperfunctions.sh

# source configuration
. ../SAMBA4.conf

printAndLogMessage "SET STATIC IP IN /etc/hosts"
## manipulated file /etc/hosts
FILE="/etc/hosts"

printAndLogMessage "Manipulated file: " ${FILE}
printAndLogMessage "Save original file: " ${FILE}
saveOriginal ${FILE}
logFile ${FILE}

printAndLogMessage "Change file: " ${FILE}
# set 127.0.1.1 to STATIC IP
sed -e "{
    /$(hostname)/ s/127.0.1.1/${THIS_SERVER_STATIC_IP}/
}" -i ${FILE}

logFile ${FILE}
