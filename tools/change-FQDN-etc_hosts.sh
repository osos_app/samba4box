#!/bin/bash

# source helper functions
. ../tools/helperfunctions.sh

# source configuration
. ../SAMBA4.conf

printAndLogMessage "SET FQDN IN /etc/hosts"
## manipulated file /etc/hosts
FILE="/etc/hosts"

printAndLogMessage "Manipulated file: " ${FILE}
printAndLogMessage "Save original file: " ${FILE}
saveOriginal ${FILE}
logFile ${FILE}

printAndLogMessage "Change file: " ${FILE}
# block host entrance, if one exists
sed -e "{
    /$(hostname)/ s/$(hostname)/$(hostname).${SAMBA4_DNS_DOMAIN_NAME} $(hostname)/
}" -i ${FILE}

logFile ${FILE}
