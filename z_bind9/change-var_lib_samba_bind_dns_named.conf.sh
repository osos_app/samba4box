#!/bin/bash

# source helper functions
. ../tools/helperfunctions.sh

# source configuration
. ../SAMBA4.conf

printAndLogMessage "CONNECT samba4 to bind9"
printAndLogMessage "uncomment library ${DLZ_BIND9_LIBRARY} in /var/lib/samba/bind-dns/named.conf"
## manipulated file /var/lib/samba/bind-dns/named.conf
FILE="/var/lib/samba/bind-dns/named.conf"

printAndLogMessage "Manipulated file: " ${FILE}
printAndLogMessage "Save original file: " ${FILE}
saveOriginal ${FILE}
logFile ${FILE}

printAndLogMessage "Change file: " ${FILE}
# block host entrance, if one exists
sed -e "{
    /${DLZ_BIND9_LIBRARY}/ s/#//
}" -i ${FILE}

logFile ${FILE}
